package com.epam.user.test.service;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import com.epam.user.entity.User;
import com.epam.user.exception.UserNotFoundException;
import com.epam.user.model.UserDto;
import com.epam.user.repository.UserRepository;
import com.epam.user.service.impl.UserServiceImpl;
import com.epam.user.utils.Converter;

@ExtendWith(SpringExtension.class)
class UserServiceTest {
	@InjectMocks
	UserServiceImpl userService;
	
	@Mock
	Converter converter;	
	
	@Mock
	private UserRepository userRepository;
	
	List<UserDto> usersDto;
	List<User> users;
	
	UserDto userDto;
	User user;
	@BeforeEach
	void setUp() throws Exception {
		MockitoAnnotations.initMocks(this);
		user = new User(1L,"Mike","horror@dg","abak");
		 users = new ArrayList<>();
		users.add(user);
		
		userDto = new UserDto(1L,"Mike","horror@dg","abak");
		usersDto = new ArrayList<>();
		usersDto.add(userDto);
	}
	@Test
	void testGetAllUser() {
		when(converter.covertEntityToDto(user)).thenReturn(userDto);
		when(userRepository.findAll()).thenReturn(users);
		assertEquals("Mike",userService.getAll().get(0).getName());
		verify(userRepository).findAll();
	}

	@Test
	void testGetUserById() {
		when(converter.covertEntityToDto(user)).thenReturn(userDto);
		when(userRepository.findById(1L)).thenReturn(Optional.of(user));
		assertEquals("Mike",userService.getById(1L).getName());
		verify(userRepository).findById(1L);
	}
	@Test
	void testGetUserByIdInvalid() {
		when(converter.covertEntityToDto(user)).thenReturn(userDto);
		when(userRepository.findById(12L)).thenReturn(Optional.empty());
		assertThrows(UserNotFoundException.class,()->userService.getById(12L));
		verify(userRepository).findById(12L);
	}

	@Test
	void testAddUser() {
		when(converter.covertDtoToEntity(userDto)).thenReturn(user);
		when(converter.covertEntityToDto(Mockito.any(User.class))).thenReturn(userDto);
		when(userRepository.save(Mockito.any(User.class))).thenReturn(user);
		assertEquals("Mike",userService.addNew(userDto).getName());
		verify(userRepository).save(user);
	}

	@Test
	void testUpdateUser() {
		UserDto updatedUserDto = new UserDto(1L,"Mike De","horror@dgupdate","abak");
		User updatedUser = new User(1L,"Mike De","horror@dgupdate","abak");
		when(converter.covertDtoToEntity(updatedUserDto)).thenReturn(updatedUser);
		when(converter.covertEntityToDto(updatedUser)).thenReturn(updatedUserDto);
		when(userRepository.findById(1L)).thenReturn(Optional.of(user));
		when(userRepository.save(user)).thenReturn(updatedUser);
		assertEquals("Mike De",userService.update(1L, updatedUserDto).getName());
		verify(userRepository).findById(1L);
		verify(userRepository).save(user);	}

	@Test
	void testRemoveUser() {
		when(userRepository.findById(1L)).thenReturn(Optional.of(user));
		doNothing().when(userRepository).delete(user);
		userService.remove(1L);;
		verify(userRepository).delete(user);
	}

}
