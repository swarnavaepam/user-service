package com.epam.user.test.controller;

import static org.hamcrest.CoreMatchers.is;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.BDDMockito.given;
import static org.mockito.BDDMockito.willDoNothing;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.header;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;

import com.epam.user.controller.UserController;
import com.epam.user.model.UserDto;
import com.epam.user.repository.UserRepository;
import com.epam.user.service.impl.UserServiceImpl;

@WebMvcTest(UserController.class)
class UserControllerTest {

	@Autowired
	MockMvc mvc;
	
	@MockBean
	UserServiceImpl userService;
	
	UserRepository userRep;
	List<UserDto> usersDto;
	
	UserDto userDto;
	@BeforeEach
	void setUp() throws Exception {
		userDto = new UserDto(1L,"Mike","horror@dg","abak");
		usersDto = new ArrayList<>();
		usersDto.add(userDto);
	}

	@Test
	void testFetchAllUserInvalid() throws Exception {
		given(this.userService.getAll()).willReturn(Collections.emptyList());
	    this.mvc.perform(get("/user").accept(MediaType.APPLICATION_JSON))
	            .andExpect(status().isNotFound());
	}
	@Test
	void testFetchAllUser() throws Exception {
		given(this.userService.getAll()).willReturn(usersDto);
	    this.mvc.perform(get("/user").accept(MediaType.APPLICATION_JSON))
	            .andExpect(status().isOk())
	            .andExpect(content().contentType(MediaType.APPLICATION_JSON));
	}

	@Test
	void testFetchUser() throws Exception {
		given(userService.getById(1L)).willReturn(userDto);
		this.mvc.perform(get("/user/1").accept(MediaType.APPLICATION_JSON))
				.andExpect(status().isOk())
				.andExpect(content().contentType(MediaType.APPLICATION_JSON))
				.andExpect(jsonPath("$.name",is("Mike")));
	}

	@Test
	void testAddUser() throws Exception {
		given(userService.addNew(any(UserDto.class))).willReturn(userDto);
		String userJson = "{\r\n" + 
				"  \"name\": \"Mike\",\r\n" + 
				"  \"email\": \"string\",\r\n" + 
				"  \"password\": \"string\"\r\n" + 
				"}";
	    MockHttpServletRequestBuilder request = post("/user")
	            .content(userJson)
	            .contentType(MediaType.APPLICATION_JSON)
	            .accept(MediaType.APPLICATION_JSON);
		this.mvc.perform(request)
				.andExpect(status().isCreated())
				.andExpect(content().contentType(MediaType.APPLICATION_JSON))
				.andExpect(header().exists("Location"))
	            .andExpect(jsonPath("$.name", is("Mike")));
	}

	@Test
	void testUpdate() throws Exception {
		UserDto updatedUserDto = new UserDto(1L,"Mike De","horror@dgupdate","abak");
		given(userService.update(Mockito.anyLong(),any(UserDto.class))).willReturn(updatedUserDto);
		String userJson = "{\r\n" + 
				"  \"name\": \"Mike De\",\r\n" + 
				"  \"email\": \"horror@dgupdate\",\r\n" + 
				"  \"password\": \"abak\"\r\n" + 
				"}";
	    MockHttpServletRequestBuilder request = put("/user/1")
	            .content(userJson)
	            .contentType(MediaType.APPLICATION_JSON)
	            .accept(MediaType.APPLICATION_JSON);
		this.mvc.perform(request)
				.andExpect(status().isCreated())
				.andExpect(content().contentType(MediaType.APPLICATION_JSON))
				.andExpect(header().exists("Location"))
	            .andExpect(jsonPath("$.name", is("Mike De")));
		
		
	}

	@Test
	void testDelete() throws Exception {
		willDoNothing().given(this.userService).remove(1L);;
		this.mvc.perform(delete("/user/1"))
		.andExpect(status().isNoContent());
	}

}
