package com.epam.user.controller;

import java.net.URI;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.epam.user.exception.UserNotFoundException;
import com.epam.user.model.UserDto;
import com.epam.user.service.UserService;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@RestController
@RequestMapping("/user")
public class UserController {
	
	@Autowired
	UserService userService;

	private static final Logger log = LogManager.getLogger(UserController.class);

	
	@GetMapping
	@ApiOperation(value="Used to get all the users",
				 notes="It returns a NOTFOUND status code is no user exist.")
	@ApiResponses(value = {@ApiResponse( code = 200, message = "Received all users successfully"),
						  @ApiResponse(code = 404, message = "No user received")})
	public ResponseEntity<?> fetchAllUser(){
		
		log.info("fetching all user");
		List<UserDto> users = userService.getAll();
		if(users.isEmpty()) {
			
			return ResponseEntity.notFound().build();
		}
		return ResponseEntity.ok(users);
	}
	
	@GetMapping("{id}")
	@ApiOperation(value="Used to get a particular the user by id",
				  notes="It returns a NOTFOUND status code if user does not exist.")
	@ApiResponses(value = {@ApiResponse( code = 200, message = "Received the user successfully"),
			  @ApiResponse(code = 404, message = "No user received", response=UserNotFoundException.class)})
	public ResponseEntity<UserDto> fetchUser(@PathVariable Long id){
		
		log.info("Fetching an user");
		return ResponseEntity.ok(userService.getById(id));
		
	}
	
	@PostMapping
	@ApiOperation(value="Used to create a new user record",
				  notes="It takes the user model in a JSON format and creates a new record.")
	@ApiResponses(value = {@ApiResponse( code = 201, message = "New user created successfully")})
	public ResponseEntity<?> addUser(@RequestBody UserDto userDto){
		log.info("Creating new user");
		UserDto savedUser = userService.addNew(userDto);
		URI location = ServletUriComponentsBuilder.fromCurrentRequest()
	            .path("/{id}")
	            .buildAndExpand(savedUser.getId())
	            .toUri();

		return ResponseEntity.created(location).body(savedUser);
	}
	
	@PutMapping("{id}")
	@ApiOperation(value="Used to create a new user record",
	  				notes="It returns a NOTFOUND status code if user does not exist.")
	@ApiResponses(value = {@ApiResponse(code = 201, message = "New user created successfully"),
						   @ApiResponse(code = 404, message = "If the user with the id does not exist.")})
	public ResponseEntity<?> update(@RequestBody UserDto userDto,
									@PathVariable Long id){
		log.info("Updating User");
		URI location = URI.create(ServletUriComponentsBuilder.fromCurrentRequest().toUriString());
		return ResponseEntity.created(location).body(userService.update(id, userDto));
		
	}
	
	@DeleteMapping("{id}")
	@ApiOperation(value = "For delete an user with a particular id",
				  notes = "It takes the id of the user to be deleted, and deletes the user data"
						  + "with that id. If the id does not exist, then UserNotFoundException"
						  + "is thrown. On successful deletion it has status code NoContent")
	@ApiResponses(value = {@ApiResponse(code = 204, message = "An User is successfully deleted and RespnseBody has no content"),
					       @ApiResponse(code = 404, message = "user id does not exist")})
	public ResponseEntity<?> delete(@PathVariable Long id){
		log.info("removing user");
		userService.remove(id);
		return ResponseEntity.noContent().build();
	}
}
