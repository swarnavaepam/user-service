package com.epam.user.service.impl;

import java.util.List;
import java.util.Optional;
import static java.util.stream.Collectors.toList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.epam.user.entity.User;
import com.epam.user.exception.UserNotFoundException;
import com.epam.user.model.UserDto;
import com.epam.user.repository.UserRepository;
import com.epam.user.service.UserService;
import com.epam.user.utils.Converter;

@Service
public class UserServiceImpl implements UserService {
	
	UserRepository userRepository;
	
	@Autowired
	Converter converter;
	
	@Autowired
	 public UserServiceImpl(UserRepository userRepository) {
	    this.userRepository = userRepository;
	  }
	 
	@Override
	public List<UserDto> getAll() {
		List<User> users =(List<User>)userRepository.findAll();
			return	users.stream()
				 .map(converter::covertEntityToDto)
				 .collect(toList());		
	} 

	@Override
	public UserDto getById(Long id) {
		Optional<User> user = userRepository.findById(id);
		if(user.isPresent()) {
			return converter.covertEntityToDto(user.get());
		}
		throw new UserNotFoundException("Not found");
	}

	@Override
	public UserDto addNew(UserDto userDto) {
		User user = converter.covertDtoToEntity(userDto);
		
		return converter.covertEntityToDto(userRepository.save(user));
	}

	@Override
	public UserDto update(Long id, UserDto userDto) {
		Optional<User> user = userRepository.findById(id);
		if (user.isPresent()) {
			
			user.get().setName(userDto.getName());
			user.get().setPassword(userDto.getPassword());
			user.get().setEmail(userDto.getEmail());
			
			return converter.covertEntityToDto(userRepository.save(user.get()));
		}
		throw new UserNotFoundException("Not found");
	}

	@Override
	public void remove(Long id) {
		Optional<User> user = userRepository.findById(id);
		if(user.isPresent()) {
			
			userRepository.delete(user.get());
		}
		else {
			
			throw new UserNotFoundException("Not found");
		}
	}
	

}
