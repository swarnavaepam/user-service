package com.epam.user.service;

import java.util.List;

import com.epam.user.model.UserDto;

public interface UserService {
	List<UserDto> getAll();
	UserDto getById(Long id);
	UserDto addNew(UserDto userDto);
	UserDto update(Long id,UserDto userDto);
	void remove(Long id);
}
 