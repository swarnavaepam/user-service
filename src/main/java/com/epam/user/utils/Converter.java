package com.epam.user.utils;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.epam.user.entity.User;
import com.epam.user.model.UserDto;

@Component
public class Converter {
	@Autowired
	ModelMapper modelMapper;

	public User covertDtoToEntity(UserDto userDto) {
		return modelMapper.map(userDto,User.class);
	}
	public UserDto covertEntityToDto(User user) {
		return modelMapper.map(user,UserDto.class);
	}
	
}
