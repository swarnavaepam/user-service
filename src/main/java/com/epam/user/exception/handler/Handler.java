package com.epam.user.exception.handler;

import java.time.LocalDateTime;

import javax.servlet.http.HttpServletRequest;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import com.epam.user.exception.UserNotFoundException;
import com.epam.user.model.ExceptionDto;

@RestControllerAdvice
public class Handler {
	
	@ExceptionHandler(value = UserNotFoundException.class)
	public ResponseEntity<ExceptionDto> handeUserNotFoundException(HttpServletRequest request,UserNotFoundException exception) {
		ExceptionDto exceptionDto = new ExceptionDto("No such User found", LocalDateTime.now());
		return ResponseEntity.status(HttpStatus.NOT_FOUND).body(exceptionDto);
	}

}
