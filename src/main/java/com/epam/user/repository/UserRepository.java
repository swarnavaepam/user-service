package com.epam.user.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.epam.user.entity.User;

@Repository
public interface UserRepository extends CrudRepository<User, Long> {

}
 